# icanhazk8s.com

CAN I HAZ KUBERNETES CLUSTER?

This site collects the set of links and resources I found useful while learning Kubernetes as well as notes I took. I hope this information is useful for you on your journey towards a better understanding of Kubernetes.

![kubecat](kubecat.png)


## Fundamental Terms
I am working on a series of posts describing the [Fundamental terms](https://kubernetes.io/docs/reference/glossary/?fundamental=true) from the Kubernetes Standardized Glossary. So far, I have covered:

  - [cluster, node and pod](notes/2020-11-fundamental-k8s-terms-pod-node-cluster.md)
  - [kubectl and minikube](notes/2020-11-fundamental-k8s-terms-kubectl-minikube.md)


## Podcast Episodes

Here are the specific podcast episodes that I found useful:

  - [Minikube Redux, with Thomas Strömberg](https://kubernetespodcast.com/episode/115-minikube-redux/)

## Comics
  - [The Illustrated Children's Guide to Kubernetes](https://www.cncf.io/the-childrens-illustrated-guide-to-kubernetes/)
  - [Smooth Sailing with Kubernetes](https://cloud.google.com/kubernetes-engine/kubernetes-comic/)