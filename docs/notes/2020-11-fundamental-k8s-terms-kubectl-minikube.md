# Fundamental K8s Terms, Part 02: Kubectl and Minikube

This is an installment in the **Fundamental K8s Terms** series. Feel free to check out [the previous installment](https://icanhazk8s.com/post/fundamental-k8s-terms-pod-node-cluster/) where we cover **cluster**, **node** and **pod**. In today's installment, we are going to cover `kubectl` and `minikube`:

  - `kubectl`: the client for the Kubernetes API, Swiss Army knife that performs any Kubernetes operation 
  - `minikube`: a tool that lets you spin up a local Kubernetes cluster

### kubectl

The Collabnix [tutorial on kubectl](https://dockerlabs.collabnix.com/kubernetes/beginners/what-is-kubect.html) puts it this way:

> From a user’s point of view, kubectl is your cockpit to control Kubernetes. It allows you to perform every possible Kubernetes operation.
> 
> From a technical point of view, kubectl is a client for the Kubernetes API.

This [table of kubectl operations](https://kubernetes.io/docs/reference/kubectl/overview/#operations) gives a good idea of the kinds of operations that can be performed via `kubectl`. Here are just a few examples:

  - apply configuration changes
  - copy files and directories to and from containers
  - drain nodes for maintenance
  - print logs for a container in a pod
  - run a specific image on a cluster
  - watch resource usage with `kubectl top`

The `kubectl` command uses the concept of contexts in order to determine which cluster it sends commands to. This is helpful for situations where you have more than one cluster. 

**READ** [Configure Access to Multiple Clusters](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/)

**READ** [intro to kubectl](https://kubectl.docs.kubernetes.io/guides/introduction/kubectl/) from the Kubectl (and Kustomize) book

#### Pronunciation
A lot of people are very concerned with how `kubectl` is pronounced. The two main camps are:

  - "cube control"
  - "cube cuttle" (kind of like 'cuddle' but the sound made by the "dd" is softer) 

You can read and watch more about it:

  - [http://www.howtopronounce.cc/kubectl](http://www.howtopronounce.cc/kubectl)
  - [https://github.com/saada/kubectl-pronounciation](https://github.com/saada/kubectl-pronounciation)
  - [kubectl:: The definitive pronunciation guide](https://www.youtube.com/watch?v=2wgAIvXpJqU&feature=emb_title)


### minikube

> minikube quickly sets up a local Kubernetes cluster on macOS, Linux, and Windows. 

`minikube` is a tool that lets you have a single node Kubernetes cluster on your local computer. Yep, it's **true**. With `minikube`, you can have a local Kubernetes cluster. My first (second, third, and fourth) Kubernetes cluster was spun up via `minikube`. Minikube is good because it let's you get started quickly. It is important to note that there are limitations and `minikube` is not intended for production use. 


#### Learn More

I highly recommend listening to the [Minikube Redux, with Thomas ](https://kubernetespodcast.com/episode/115-minikube-redux/) episode of [Kubernetes Podcast from Google](https://kubernetespodcast.com/). The conversation covers improvements made to `minikube` in the prior 18 months. Hearing the pain points that they hav emoved past provides great insight into the `minikube` experience. 

**Hello, Minikube**: Get [hands-on](https://kubernetes.io/docs/tutorials/hello-minikube/) with an in-browser Kubernetes environment via Katacoda. 

Learn more at [https://minikube.sigs.k8s.io/docs/](https://minikube.sigs.k8s.io/docs/). 
